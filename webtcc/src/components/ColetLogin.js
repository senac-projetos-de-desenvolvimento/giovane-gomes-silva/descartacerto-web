import React, { useContext } from "react";
import Conecta from "../Conecta";
import { useForm } from "react-hook-form";
import { ClienteContext } from "./ClienteContext";
import { useHistory } from "react-router-dom";

import "./ColetLogin.css";

const ColetLogin = () => {
    const {
        register,
        handleSubmit,
    } = useForm();

    let history = useHistory();
    const cliente = useContext(ClienteContext);

    const onSubmit = async (data) => {
        const login = await Conecta.post("loginColetoras", data);
        console.log(login);
        if (login.data.id) {
            cliente.setDados({
              id: login.data.id,
              nome: login.data.user,
              token: login.data.token,
            });
            history.push("/homeColetoras");
            // history.push("/estatisticas");
          } else {
            alert("Erro... Acesso Inválido");
          }
        // alert(JSON.stringify(data));
    };

    const cadastrar = () => {
        cliente.setDados({ id: null, nome: '', token: '' })
        history.push("/cadastreCidades")
    }

    return (
        <div className="row mt-5">
            <div className="col-md-5 col-sm-8 col-11 mx-auto">
                <form className="form-signin" onSubmit={handleSubmit(onSubmit)}>
                    <div className="text-center mb-4">
                        <h1 className="h3 mb-3 font-weight-normal">Login de Coletoras</h1>
                        <p>Faça seu Cadastro de Ajude a Mudar o Mundo</p>
                    </div>

                    <div className="form-label-group">
                        <input
                            type="text"
                            id="cnpj_cpf"
                            className="form-control"
                            placeholder="E-mail do Cliente"
                            required
                            autoFocus
                            {...register("cnpj_cpf")}
                        />
                        <label htmlFor="cnpj">CNPJ da Empresa</label>
                    </div>

                    <div className="form-label-group">
                        <input
                            type="password"
                            id="senha_coletora"
                            className="form-control"
                            placeholder="Senha de Acesso"
                            required
                            {...register("senha_coletora")}
                        />
                        <label htmlFor="senha">Senha de Acesso</label>
                    </div>

                    <button className="btn btn-lg btn-primary btn-block" type="submit">
                        Entrar
                    </button>

                    <button className="btn btn-lg botao-cadastrar btn-block" type="submit" onClick={cadastrar}>
                        Fazer Cadastro
                    </button>
                </form>
            </div>
        </div>
    );
};

export default ColetLogin;