import React, { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import Conecta from "../Conecta";
import ItemLista from "./ItemLista";
import { useHistory } from "react-router-dom";

const ListaColetoras = () => {
    const [coletoras, setColetoras] = useState([]);

   

    return (
        <div>
                       
            <div className="row">
                {coletoras.map((coletora, index) => (
                    <ItemLista
                        foto={coletora.foto}
                        nome_coletora={coletora.nome_coletora}
                        endereco_coletora={coletora.endereco_coletora}
                        numero_coletora={coletora.numero_coletora}
                        bairro_coletora={coletora.bairro_coletora}
                        telefone={coletora.telefone}
                        // likes={coletora.likes}
                        // likeClick={() => clienteLike(coletora.id, index)}
                        // dislikes={coletora.deslikes}
                        // dislikeClick={() => clienteDislike(coletora.id)}
                        key={coletora.id_coletora}
                    />
                ))}
            </div>
            
        </div>

        


    );

};



export default ListaColetoras;