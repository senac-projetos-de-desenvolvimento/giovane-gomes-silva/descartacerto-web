import React, { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import Conecta from "../Conecta";
import ItemLista from "./ItemLista";
import "./Home.css"
import { useHistory } from "react-router-dom";

const FormHome = () => {
    // const [cidade, setCidade] = useState('')
    const [especialidades, setEspecialidades] = useState([]);
    const [cidades, setCidades] = useState([]);
    const [coletoras, setColetoras] = useState([]);
    const [erro, setErro] = useState(false);

    const {
        register,
        handleSubmit,
        formState: { errors },
        setValue

    } = useForm();

    useEffect(() => {
        getcidades();
        getespecialidades();
    }, []);

    let history = useHistory();

    const onSubmit = async (data) => {

        // const data = {
        //     cidades_id_cidades,
        //     especialidades_id_especialidades
        // }

        // console.log(data.cidades_id_cidades);
        await Conecta.get(`/coletoras/${data.cidades_id_cidades}/${data.especialidades_id_especialidades}`).then((response) => {
            setErro(true)
            setColetoras(response.data)
            console.log(response.data);
            // if(setColetoras(response.data.length) === 0){

            //     setErro(false)
            // }
        });


        // history.push("/listaColetoras")

    };

    const getespecialidades = async () => {
        const lista = await Conecta.get("especialidades");
        console.log(lista);

        setEspecialidades(lista.data);
    };

    const getcidades = async () => {
        const lista = await Conecta.get("cidades");
        console.log(lista);

        setCidades(lista.data);
    };

    // const onSubmitEspecialidades = async (data) => {

    //     await Conecta.post("especialidades", data);

    //     history.push("/cadastreColetoras")

    // };

    return (
        <div className="homePrinci">
            <h1 className="text-center mt-3"> Faça sua Pesquisa e Encontre Pontos de Coletas</h1>
            <div className="busca">
                <form className="row g-3 justify-content-center my-5 pesq" onSubmit={handleSubmit(onSubmit)}>
                    <div className="col-md-3">
                        Selecione a cidade:

                        <select
                            className="ml-3"
                            {...register("cidades_id_cidades")}
                        >
                            {cidades.map((cidade, index) => (
                                <option key={cidade.id} value={cidade.id_cidades}>{cidade.nome_cidade}</option>
                            ))}

                        </select>
                    </div>

                    <div className="col-md-3">
                        Selecione a Especialidade:
                        <select
                            className="ml-3"
                            {...register("especialidades_id_especialidades")}
                        >
                            {especialidades.map((especialidade, index) => (
                                <option key={especialidade.id_especialidades} value={especialidade.id_especialidades}>{especialidade.nome_especialidades}</option>
                            ))}

                        </select>
                    </div>

                    <input
                        type="submit"
                        value="Pesquisar"
                        className="btn btn-success col-md-8 mb-1"
                    />
                </form>
            </div>

            <div className="container-fluid home-bg">

                <div className="row">
                    {/* {erro && <p>{coletoras.email_coletora}</p>} */}
                    {coletoras.map((coletora, index) => (
                        <ItemLista
                            foto={coletora.foto}
                            nome_coletora={coletora.nome_coletora}
                            endereco_coletora={coletora.endereco_coletora}
                            numero_coletora={coletora.numero_coletora}
                            bairro_coletora={coletora.bairro_coletora}
                            telefone={coletora.telefone}
                            key={coletora.id_coletora}
                        />
                    ))}
                </div>

            </div>

        </div>


    );

};



export default FormHome;