import React, { useEffect, useState } from "react";
import Header from './components/Header';
import FormLogin from './components/FormLogin';
import FormColet from './components/FormColet';
import UserLogin from './components/UserLogin';
import ColetLogin from './components/ColetLogin';
import { ClienteContext } from "./components/ClienteContext.js"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import FormCity from "./components/FormCity";
import FormHome from "./components/Home";
import ListaColetoras from "./components/ListaColetoras";
import HomeColetoras from "./components/HomeColetoras";

function App() {

  const [dados, setDados] = useState({})

  const [cidade, setCidade] = useState({})
  const [especialidades, setEspecialidades] = useState({})

  return (
    <ClienteContext.Provider value={{ dados, setDados }}>
      <Router>
        <Header />
        <Switch>
          <Route path="/" exact >
            <FormHome />
          </Route>
          <Route path="/cadastreCidades" >
            <FormCity />
          </Route>
          <Route path="/homeColetoras" >
            <HomeColetoras />
          </Route>
          <Route path="/cadastreColetoras" >
            <FormColet />
          </Route>
          <Route path="/loginColetoras">
            <ColetLogin />
          </Route>
          <Route path="/cadastreUsuario" >
            <FormLogin />
          </Route>
          <Route path="/login">
            <UserLogin />
          </Route>
        </Switch>
      </Router>
    </ClienteContext.Provider>
  );
}

export default App;
