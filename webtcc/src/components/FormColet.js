import React, { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import "./FormLogin.css"
import Conecta from "../Conecta";
import { useHistory } from "react-router-dom";

const Froms = () => {
    const [cidades, setCidades] = useState([]);
    const [cidad, setCidad] = useState("");
    const [especialidades, setEspecialidades] = useState([]);
    const [especialidad, setEspecialidad] = useState("");
    // const [value, setValue] = useState([]);

    const {
        register,
        handleSubmit,
        formState: { errors },


    } = useForm();

    let history = useHistory();

    const onSubmit = async (data) => {


        // const tes = {
        //     // numero_coletora: numero_coletora,
        //     especialidades_id_especialidades: Number(especialidad),
        //     cidades_id_cidades: Number(cidad),
        // }
        data = { ...data, especialidades_id_especialidades: Number(especialidad), cidades_id_cidades: Number(cidad) }

        // data.push(tes)

        console.log(data);
        await Conecta.post("coletoras", data);
        alert("Usuário Cadastrado com sucesso!!" + `Bem vindo ${data.nome_coletora}`);

        history.push("/loginColetoras")

        // limpa os campos de formulário
        // setValue("Test", "");
        // setValue("genero", "");
        // setValue("cidade", "");
        // setValue("endereco", "");
        // setValue("numero", "");
        // setValue("complemento", "");
        // setValue("bairro", "");
        // setValue("telefone", "");
        // setValue("email", "");
        // setValue("password", "");

    };

    const getcoletoras = async () => {
        const lista = await Conecta.get("cidades");
        console.log(lista);

        setCidades(lista.data);
    };

    const getespecialidades = async () => {
        const lista = await Conecta.get("especialidades");
        console.log(lista);

        setEspecialidades(lista.data);
    };

    // getcoletoras()
    useEffect(() => {
        getcoletoras();
        getespecialidades();
    }, []);


    return (
        <div>
            <h1 className="text-center mt-3">Cadastre seu Centro de Reciclagem!</h1>
            <form className="row g-3 justify-content-center mt-5" onSubmit={handleSubmit(onSubmit)}>
                <div className="col-md-4">

                    <input
                        type="text"
                        className="form-control"
                        placeholder="Nome da Coletora"
                        {...register("nome_coletora", {
                            required: true,
                            minLength: 3,
                        })}
                    />
                </div>
                <div className="col-md-4">
                    Selcione a Especialidade:
                    {/* USAR O ...register ver documentação */}
                    <select

                        onChange={(e) => setEspecialidad(e.target.value)}
                        className="ml-1"
                    >

                        {especialidades.map((especialidade, index) => (
                            <option key={especialidade.id_especialidades} value={especialidade.id_especialidades}>{especialidade.nome_especialidades}</option>
                        ))}


                    </select>




                </div>
                <div className="col-md-6">
                    {/* <div className="input-group-prepend">
                    <span className="input-group-text">
                        <i className="fa fa-address-card text-primary py-1"></i>
                    </span>
                </div> */}
                    <input
                        type="url"
                        className="form-control"
                        placeholder="foto"
                        {...register("foto", {
                            required: true,
                            minLength: 3,
                        })}
                    />
                </div>

                <div className="col-md-2">

                    <input
                        type="text"
                        className="form-control"
                        placeholder="Número"
                        {...register("numero_coletora", {
                            required: true,
                            minLength: 1,
                        })}
                    />
                </div>

                <div className="col-md-5">

                    <input
                        type="text"
                        className="form-control"
                        placeholder="Endereço"
                        {...register("endereco_coletora", {
                            required: true,
                            minLength: 3,
                        })}
                    />
                </div>

                <div className="col-md-3">

                    <input
                        type="text"
                        className="form-control"
                        placeholder="Complemento"
                        {...register("complemento_coletora", {
                            required: false,
                            minLength: 3,
                        })}
                    />
                </div>

                <div className="col-md-5">

                    <input
                        type="text"
                        className="form-control"
                        placeholder="Bairro"
                        {...register("bairro_coletora", {
                            required: false,
                            minLength: 3,
                        })}
                    />
                </div>

                <div className="col-md-2">
                    Selcione a Cidade:
                    <select

                        className="ml-1"
                        onChange={(e) => setCidad(e.target.value)}
                    // {...props}
                    >
                        {cidades.map((cidade, index) => (
                            <option key={cidade.id} value={cidade.id_cidades}>{cidade.nome_cidade}</option>
                        ))}

                    </select>

                    {/* <p> {console.log(cidad)}</p> */}

                    {/* <input
                    type="text"
                    className="form-control"
                    placeholder="Cidade"
                    {...register("target", {
                        required: true,
                        // minLength: 3,
                    })}
                /> */}
                </div>

                <div className="col-md-1">

                    <input
                        type="text"
                        className="form-control"
                        placeholder="Estado"
                        {...register("estado_coletora", {
                            required: true,
                            minLength: 3,
                        })}
                    />
                </div>

                <div className="col-md-5">

                    <input
                        type="text"
                        className="form-control"
                        placeholder="telefone"
                        {...register("telefone", {
                            required: true,
                            minLength: 3,
                        })}
                    />
                </div>

                <div className="col-md-3">

                    <input
                        type="text"
                        className="form-control"
                        placeholder="CNPJ"
                        {...register("cnpj_cpf", {
                            required: true,
                            minLength: 8,
                        })}
                    />
                </div>

                <div className="col-md-8">

                    <input
                        type="email"
                        className="form-control"
                        placeholder="Email"
                        {...register("email_coletora", {
                            required: true,
                            minLength: 3,
                        })}
                    />
                </div>

                <div className="col-md-8">

                    <input
                        type="password"
                        className="form-control"
                        placeholder="Senha"
                        {...register("senha_coletora", {
                            required: true,
                            minLength: 3,
                        })}
                    />
                </div>
                <div className="form-check-inline col-md-8">
                    <label className="form-check-label mr-2 my-1">
                        <input type="radio" className="form-check-input" name="optradio" /> Público
                    </label>

                    {/* <div className="form-check-inline"> */}
                    <label className="form-check-label">
                        <input type="radio" className="form-check-input" name="optradio" /> Privado
                    </label>
                </div>

                <div className="form-check col-md-8">
                    <label className="form-check-label">
                        <input type="checkbox" className="form-check-input" value="" />Li e concordo com os termos de uso e politica de privacidade
                    </label>
                </div>
                <div className="form-check col-md-8">
                    <label className="form-check-label">
                        <input type="checkbox" className="form-check-input" value="" /> Desejo receber a newsletter da Descarta Certo por email
                    </label>
                </div>

                <input
                    type="submit"
                    value="Cadastrar"
                    className="btn btn-success col-md-8"
                />

                <div
                    className={
                        (errors.Test || errors.cidade || errors.endereco || errors.genero || errors.numero || errors.numero || errors.numero) &&
                        "alert alert-danger mt-1"
                    }
                >
                    {errors.Test && (
                        <span>Preencha seu Test corretamente; </span>
                    )}
                    {errors.cidade && <span>Informe a cidade; </span>}
                    {errors.endereco && <span>Preencha o endereco</span>}
                    {errors.genero && <span>Insira o genero</span>}
                    {errors.image && <span>Preencha a image do imóvel</span>}
                </div>

            </form>
        </div>

    );

};



export default Froms;