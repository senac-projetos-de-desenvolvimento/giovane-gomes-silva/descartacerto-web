import React from "react";
import { useForm } from "react-hook-form";
import Conecta from "../Conecta";
import { useHistory } from "react-router-dom";

const FormCity = () => {
    // const [cidade, setCidade] = useState('')

    const {
        register,
        handleSubmit,
        formState: { errors },
        setValue

    } = useForm();

    let history = useHistory();

    const onSubmit = async (data) => {

        await Conecta.post("cidades", data);

        // history.push("/cadastreColetoras")

    };

    const onSubmitEspecialidades = async (data) => {

        await Conecta.post("especialidades", data);

        history.push("/cadastreColetoras")

    };

    return (
        <div>
            <form className="row mt-5 justify-content-center" onSubmit={handleSubmit(onSubmit)}>
                <div className="col-md-6 mb-1">

                    <input
                        type="text"
                        className="form-control"
                        placeholder="Cidade"
                        {...register("nome_cidade", {
                            required: true,
                            minLength: 3,
                        })}
                    />
                </div>
                <p></p>
                <input
                    type="submit"
                    value="Cadastrar"
                    className="btn btn-success col-md-6"
                />
            </form>

            <form className="row mt-5 justify-content-center" onSubmit={handleSubmit(onSubmitEspecialidades)}>
                <div className="col-md-6 mb-1">

                    <input
                        type="text"
                        className="form-control"
                        placeholder="Especialidade"
                        {...register("nome_especialidades", {
                            required: false,
                            minLength: 3,
                        })}
                    />
                </div>
                <p></p>

                <input
                    type="submit"
                    value="Cadastrar"
                    className="btn btn-success  col-md-6"
                />
            </form>

        </div>


    );

};



export default FormCity;