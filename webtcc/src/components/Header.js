import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { ClienteContext } from "./ClienteContext";
// import { Link } from "react-router-dom";
import "./Header.css";
import { useHistory } from "react-router-dom";


const Header = () => {
    const cliente = useContext(ClienteContext);

    let history = useHistory();

    const loginLogout = () => {
        cliente.setDados({ id: null, nome: '', token: '' })
        history.push("/login")
    }

    const loginLogoutColet = () => {
        cliente.setDados({ id: null, nome: '', token: '' })
        history.push("/loginColetoras")
    }

    // const esta = () => {
    //     history.push("/estatisticas")
    // }

    return (
        <nav className="navbar navbar-expand-sm bg-primary navbar-dark">
            <Link to="/" className="navbar-brand">
                <img
                    src="lixo.jpg"
                    alt="descartaCerto"
                    width="100"
                    className="float-left mr-2"
                />
                <h3>Descarta Certo</h3>
                {/* <h5>Imóveis em Destaque</h5> */}
            </Link>


            <ul className="navbar-nav ml-auto">
                {/* <li className="nav-item">
                    <Link to="/estatisticas" className="navbar-nav ml-auto">
                        <a className="nav-link" onClick={esta}>
                            Estatisticas


                        </a>
                    </Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/graph1">
                        Gráfico de Corretores

                    </Link>
                </li> */}
                <li className="nav-item">
                    <span className="nav-link" onClick={loginLogoutColet}>
                        {/* <i class="far fa-trash-alt"></i> */}

                        {cliente.dados.nome ? cliente.dados.nome + " (Sair)" : "(Entre/Login de Coletoras)"}
                    </span>
                </li>

                <li className="nav-item">
                    <span className="nav-link" onClick={loginLogout}>
                        {/* <i className="fas fa-user-friends mr-2"></i> */}

                        {cliente.dados.nome ? cliente.dados.nome + " (Sair)" : "(Entre/Login de Usuário)"}
                    </span>
                </li>
            </ul>

        </nav>
    );
};

export default Header;