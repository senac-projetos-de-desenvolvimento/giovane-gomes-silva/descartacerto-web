import React from "react";
import { useForm } from "react-hook-form";
import "./FormLogin.css"
import Conecta from "../Conecta";



const Froms = () => {

    const {
        register,
        handleSubmit,
        formState: { errors },
        setValue

    } = useForm();

    const onSubmit = async (data) => {

        console.log(data)
        await Conecta.post("usuarios", data);
        alert("Usuário Cadastrado com sucesso!!" + `Bem vindo ${data.nome}`);

        // limpa os campos de formulário
        setValue("nome", "");
        setValue("genero", "");
        setValue("cidade", "");
        setValue("endereco", "");
        setValue("numero", "");
        // setValue("complemento", "");
        setValue("bairro", "");
        setValue("telefone", "");
        setValue("email", "");
        setValue("password", "");

    };

    return (
        <div>
            <h1 className="text-center mt-3">Cadastre e Agende sua Coleta</h1>
            <form className="row mt-5 justify-content-center conta" onSubmit={handleSubmit(onSubmit)}>
                {/* <div className="container"> */}
                <div className=" row col-md-8">
                    <div className="col-md-8 my-1">

                        <input
                            type="text"
                            id="nome"
                            className="form-control"
                            placeholder="Nome Completo"
                            {...register("nome", {
                                required: true,
                                minLength: 3,
                            })}
                        />
                    </div>

                    <div className="col-md-4 my-1">

                        <input
                            type="text"
                            id="genero"
                            className="form-control"
                            placeholder="Genero"
                            {...register("genero", {
                                required: true,
                                minLength: 3,
                            })}
                        />
                    </div>

                    <div className="col-md-8 my-1">

                        <input
                            type="text"
                            id="endereco"
                            className="form-control"
                            placeholder="Endereço"
                            {...register("endereco", {
                                required: true,
                                minLength: 3,
                            })}
                        />
                    </div>

                    <div className="col-md-2 my-1" >

                        <input
                            type="text"
                            id="numero"
                            className="form-control"
                            placeholder="numero"
                            {...register("numero", {
                                required: false,
                                minLength: 3,
                            })}
                        />
                    </div>

                    <div className="col-md-2 my-1">

                        <input
                            type="text"
                            id="cidade"
                            className="form-control"
                            placeholder="Cidade"
                            {...register("cidade", {
                                required: true,
                                minLength: 3,
                            })}
                        />
                    </div>

                    <div className="input-group mb-1 my-1">

                        <input
                            type="text"
                            id="bairro"
                            className="form-control"
                            placeholder="Bairro"
                            {...register("bairro", {
                                required: true,
                                minLength: 3,
                            })}
                        />
                    </div>

                    <div className="input-group mb-1 my-1">

                        <input
                            type="email"
                            id="email"
                            className="form-control"
                            placeholder="Email"
                            {...register("email", {
                                required: true,
                                minLength: 3,
                            })}
                        />
                    </div>

                    <div className="input-group mb-1 my-1">

                        <input
                            type="password"
                            id="senha"
                            className="form-control"
                            placeholder="Senha"
                            {...register("senha", {
                                required: true,
                                minLength: 3,
                            })}
                        />
                    </div>


                    <input
                        type="submit"
                        value="Cadastrar"
                        className="btn btn-success btn-block my-1 btnLogin"
                    />

                    <div
                        className={
                            (errors.nome || errors.cidade || errors.endereco || errors.genero || errors.numero || errors.numero || errors.numero) &&
                            "alert alert-danger mt-1"
                        }
                    >
                        {errors.nome && (
                            <span>Preencha seu nome corretamente; </span>
                        )}
                        {errors.cidade && <span>Informe a cidade; </span>}
                        {errors.endereco && <span>Preencha o endereco</span>}
                        {errors.genero && <span>Insira o genero</span>}
                        {errors.image && <span>Preencha a image do imóvel</span>}
                    </div>
                </div>
                {/* </div> */}

            </form>
        </div>
    );

};



export default Froms;